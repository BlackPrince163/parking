package com.company;

import java.util.Scanner;

public class ParkingManager {
    public static void main(String[] args) {
        Parking parking = new Parking();
        Truck truck = new Truck();
        Cars cars = new Cars();
        Scanner scanner = new Scanner(System.in);
        System.out.println("How many places for trucks");
        int parkingPlaceTrucks = scanner.nextInt();
        int numberOfParkingPlace = 0;
        int[] parkingNumberAndPlace;
        int[] truckNumber = new int[0];
        int[] truckTime = new int[0];

        System.out.println("How many places for сars");
        int parkingPlaceCars = scanner.nextInt();
        int allAuto = parkingPlaceCars + parkingPlaceTrucks;
        System.out.println("Available commands: next (next move), free (free places)");
        System.out.println("stop (stoping)");
        /*String userResponse = scanner.nextLine();*/
        int [] moveCar = new int[0];
        int[] parkingPlaceNumber = parking.createParkingPlaceNumber(parkingPlaceCars, parkingPlaceTrucks);
        int[] parkingPlaceTime = parking.createParkingPlaceTime(parkingPlaceCars, parkingPlaceTrucks);
        boolean working = true;
        int idCar = 0;
        while (working) {
            switch (scanner.nextLine()) {
                case("next"):
                    int numberOfCars = cars.generateNumberOfCars(allAuto);

                    moveCar = cars.generateMoveCar(numberOfCars);
                    idCar++;
                    String[] idCar1 = cars.generationIdCar(numberOfCars, idCar);
                    parkingNumberAndPlace = parking.fillParkingPlace(parkingPlaceNumber, parkingPlaceTime, idCar1, moveCar, truckNumber, truckTime, parkingPlaceTrucks);

                    for (int i = 0; i < parkingNumberAndPlace.length / 2; i++) {
                        parkingPlaceNumber[i] = parkingNumberAndPlace[numberOfParkingPlace];
                        parkingPlaceTime[i] = parkingNumberAndPlace[numberOfParkingPlace + 1];
                        numberOfParkingPlace += 2;
                    }
                    numberOfParkingPlace = 0;

                    parking.showParkingPlaces(parkingPlaceNumber, parkingPlaceTime, parkingPlaceTrucks);
                    parking.arrivedTrucks(truckNumber, truckTime);

                    parking.arrivedCars(idCar1, moveCar);

                    break;
                case("free"):
                    break;
                case("stop"):
                    System.exit(0);
                    break;
            }
        }
    }
}
