package com.company;

import java.util.HashSet;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;

public class Parking {
    Random random = new Random();
    Scanner scanner = new Scanner(System.in);

    int idCar = 0;
    int numberSpaces = 0;
    int numberMove = 0;
    boolean emptyPlaces;


    /*public void on() {
        System.out.println("How many parking spaces will there be?");
        this.numberSpaces = scanner.nextInt();
        move();
    }*/


    /*public void move() {
        int division2 = numberSpaces / 2;
        Cars[] parkingPlace = new Cars[numberSpaces];
        int countCars = random.nextInt(division2) + 1;
        this.numberMove++;
        System.out.println(countCars + " cars arrived");
        for (int i = 0; i < countCars; i++) {
            int move = random.nextInt(10) + 1;
            this.idCar++;
            Cars car = new Cars();
            parkingPlace[i] = car;
            *//*System.out.println("id = " + generationIdCar() + ", move = " + move + ", numberMove = " + numberMove);*//*
        }
    }*/

    public int[] createParkingPlaceNumber(int parkingPlaceCars, int parkingPlaceTrucks) {
        int[] parkPlaceNumber = new int[parkingPlaceCars + parkingPlaceTrucks];
        for (int i = 0; i < parkingPlaceCars + parkingPlaceTrucks; i++) {
            parkPlaceNumber[i] = 0;
        }
        return parkPlaceNumber;
    }

    public int[] createParkingPlaceTime(int parkingPlaceCars, int parkingPlaceTrucks) {
        int[] parkPlaceTime = new int[parkingPlaceCars + parkingPlaceTrucks];
        for (int i = 0; i < parkingPlaceCars + parkingPlaceTrucks; i++) {
            parkPlaceTime[i] = 0;
        }
        return parkPlaceTime;
    }

    public int[] fillParkingPlace(int[] parkingPlaceNumber, int[] parkingPlaceTime,
                                  String[] carNumber, int[] moveCar, int[] truckNumber, int[] truckTime, int parkingPlaceTrucks) {
        int leftCars = 0;
        int leftTrucks = 0;

        for (int i = 0; i < parkingPlaceTime.length; i++) {
            if (parkingPlaceTime[i] != 0) {
                parkingPlaceTime[i] -= 1;
            }
        }


        for (int i = 0; i < truckTime.length; i++) {
            int fitTruck = -1;
            for (int j = 0; j < parkingPlaceTrucks; j++) {
                if (parkingPlaceTime[j] == 0) {
                    parkingPlaceTime[j] = truckTime[i];
                    parkingPlaceNumber[j] = truckNumber[i];
                    fitTruck = 0;
                    break;
                }
            }
            if (fitTruck != 0) {
                leftTrucks++;
            }
        }

        for (int i = 0; i < moveCar.length; i++) {
            int fitCar = -1;
            for (int j = parkingPlaceTrucks; j < parkingPlaceTime.length; j++) {
                if (parkingPlaceTime[j] == 0) {
                    parkingPlaceTime[j] = moveCar[i];
                    fitCar = 0;
                    break;
                }
            }
            if (fitCar != 0) {
                leftCars++;
            }
        }
        int leftTrucksFromTruckParking = 0;
        for (int i = truckTime.length - leftTrucks; i < truckTime.length; i++) {
            int fitTruck = -1;
            for (int j = parkingPlaceTrucks; j < parkingPlaceTime.length - 1; j++) {
                if (parkingPlaceTime[j] == 0 && parkingPlaceTime[j + 1] == 0) {
                    parkingPlaceTime[j] = truckTime[i];
                    parkingPlaceTime[j + 1] = truckTime[i];

                    parkingPlaceNumber[j] = truckNumber[i];
                    parkingPlaceNumber[j + 1] = truckNumber[i];
                    fitTruck = 0;
                    j++;
                    break;
                }
            }
            if (fitTruck != 0) {
                leftTrucksFromTruckParking++;
            }
        }
        leftTrucks = leftTrucksFromTruckParking;

        int minCar = 11;
        int minTruck = 11;

        if (leftCars != 0 && leftTrucks != 0) {
            System.out.println();
            int count = leftCars + leftTrucks;
            System.out.println("Didn't fit everything " + count);

            for (int i = 0; i < parkingPlaceTrucks; i++) {
                if (parkingPlaceTime[i] < minTruck) {
                    minTruck = parkingPlaceTime[i];
                }
            }
            for (int i = parkingPlaceTrucks; i < parkingPlaceTime.length; i++) {
                if (parkingPlaceTime[i] < minCar) {
                    minCar = parkingPlaceTime[i];
                }
            }
            System.out.println();

            System.out.println();
        }

        return getParking(parkingPlaceNumber, parkingPlaceTime);
    }
    public int[] getParking(int[] parkingPlaceNumber, int[] parkingPlaceTime) {

        int[] parking = new int[parkingPlaceTime.length * 2];

        int carSequence = 0;
        for (int i = 0; i < parkingPlaceTime.length; i++) {
            if (parking[carSequence] == 0 && parking[carSequence + 1] == 0) {
                parking[carSequence] = parkingPlaceNumber[i];
                parking[carSequence + 1] = parkingPlaceTime[i];
            }
            carSequence += 2;
        }
        return parking;
    }

    public void arrivedCars(String[] idCar, int[] moveCar) {
        for (int i = 0; i < moveCar.length; i++) {
            System.out.println("Number of the car: " + idCar[i]
                    + " moves " + moveCar[i] + ".");
        }
    }

    public void arrivedTrucks(int[] truckNumber, int[] truckTime) {
        for (int i = 0; i < truckTime.length; i++) {
            System.out.println("number of the truck that arrived:  " + truckNumber[i]
                    + " move:  " + truckTime[i] + ".");
        }
        System.out.println();
    }

    public void showParkingPlaces(int[] parkingPlaceNumber, int[] parkingPlaceTime, int parkingPlaceTrucks) {
        for (int i = 0; i < parkingPlaceTrucks; i++) {
            if (parkingPlaceTime[i] == 0) {
                System.out.println((i + 1) + "th place is free");
                continue;
            }
            System.out.println((i + 1) + "th place truck with number "
                    + parkingPlaceNumber[i] + " move "
                    + parkingPlaceTime[i]);
        }

        for (int i = parkingPlaceTrucks; i < parkingPlaceTime.length; i++) {

            if (parkingPlaceTime[i] == 0) {
                System.out.println((i + 1) + "rd place is free");
                continue;
            }
            if ((i != parkingPlaceTime.length - 1) && (parkingPlaceNumber[i] == parkingPlaceNumber[i + 1])) {
                System.out.println("At the " + (i + 1) + "th and " + (i + 2) + "th parking spaces, there is a truck with number "
                        + parkingPlaceNumber[i] + " move: "
                        + parkingPlaceTime[i]);
                i++;
                continue;

            }
            System.out.println((i + 1) + "th there is a car with a license plate "
                    + parkingPlaceNumber[i] + " move: "
                    + parkingPlaceTime[i]);
        }
        System.out.println();
    }
}
