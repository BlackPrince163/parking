package com.company;

import java.util.Random;

public class Cars {

    private int moves;
    private int numberMove;

    int numberOfCars;
    Random random = new Random();

    public String[] generationIdCar(int numberOfCars, int idCar) {
        String idCar1 = null;
        String[] carNumber = new String[numberOfCars];
        for (int c = 0; c < carNumber.length; c++) {
            if (idCar < 10){
                idCar1 = "000" + idCar;
            } else if (idCar >= 10 && idCar < 100) {
                idCar1 = "00" + idCar;
            } else if (idCar >= 100 && idCar < 1000) {
                idCar1 = "0" + idCar;
            } else if (idCar >= 1000 && idCar < 10000) {
                idCar1 = "" + idCar;
            }
            carNumber[c] = idCar1;
        }

        return carNumber;
    }

    public int generateNumberOfCars(int parkingPlaces){
        numberOfCars = 0;
        numberOfCars = random.nextInt(parkingPlaces/3+1);
        return (numberOfCars);
    }

    public int[] generateMoveCar(int numberOfCars){
        int[] moveCar = new int[numberOfCars];
        for (int i = 0; i < moveCar.length; i++) {
            int move = 0;
            while (move==0){
                move = random.nextInt(11);
            }
            moveCar[i] = move;
        }
        return moveCar;
    }

    public int getMoves() {
        return moves;
    }


    public void setMoves(int moves) {
        this.moves = moves;
    }
}
